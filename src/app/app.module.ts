import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormControlComponent } from './form-control/form-control.component';
import { InputComponent } from './form-control/input/input.component';
import { FormGroupComponent } from './form-group/form-group.component';

@NgModule({
  declarations: [AppComponent, FormControlComponent, InputComponent, FormGroupComponent],
  imports: [BrowserModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
