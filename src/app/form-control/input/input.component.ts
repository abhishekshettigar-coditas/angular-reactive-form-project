import { Component, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements ControlValueAccessor {
  @Input('id') id: string = '';
  @Input('label') label: string = '';
  @Input('placeholder') placeholder: string = '';
  @Input('type') type: string = '';
  @Input('name') name: string = '';

  formControl = new FormControl('', Validators.required);

  constructor() {}
  writeValue(obj: any): void {
    throw new Error('Method not implemented.');
  }
  registerOnChange(fn: any): void {
    throw new Error('Method not implemented.');
  }
  registerOnTouched(fn: any): void {
    throw new Error('Method not implemented.');
  }
  setDisabledState?(isDisabled: boolean): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {}
}
